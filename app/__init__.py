from flask import Flask, request, jsonify
from .kenzie import image
import os



app = Flask(__name__)
app.config['MAX_CONTENT_LENGTH'] = 1000000
FILES_DIRECTORY = os.environ.get('FILES_DIRECTORY')
DOWNLOAD_PATH = os.environ.get('DOWNLOAD_PATH')

'''
list_all_image - returns a list of bank images
'''
@app.get("/files")
def list_all_image():
    images_list = image.list_all()
    return jsonify(images_list), 200


'''
list_image_- returns images by the provided extension (jpg, png, gif)
'''
@app.get("/files/<string:extension>")
def list_image_(extension: str):
    image_extension = image.list_extension(extension)
    return jsonify(image_extension)


'''
upload_image - upload the image to the bank
'''
@app.post("/upload")
def upload_image():
    if "file" not in request.files:
        return {"msg": "Insira um arquivo"}, 400

    file = request.files['file']
    return image.save_image(file)
   

'''
download_file - get a file for download
'''
@app.get("/download/<string:file_name>")
def download_file(file_name: str):
    return image.download_image(file_name)


'''
download_zip - get a file in format zip and do download
'''
@app.get("/download-zip")
def download_zip():
    if str(request.args.get('file_extension')):
        file_extension = str(request.args.get('file_extension'))
        print(file_extension)
    else:
        file_extension = None
    
    if int(request.args.get('compression_ratio')):
        compression_ratio = int(request.args.get('compression_ratio'))
        print(compression_ratio)
    else:
        compression_ratio = None
    
    return image.download_zip_file(file_extension, compression_ratio)
