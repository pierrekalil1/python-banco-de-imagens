import os
import dotenv
# from pathlib import Path

dotenv.load_dotenv()

FILES_DIRECTORY = os.environ.get('FILES_DIRECTORY')
ALLOWED_EXTENSIONS = os.environ.get('ALLOWED_EXTENSIONS')

os.makedirs(FILES_DIRECTORY, exist_ok=True)
os.makedirs(f"{FILES_DIRECTORY}/png", exist_ok=True)
os.makedirs(f"{FILES_DIRECTORY}/gif", exist_ok=True)
os.makedirs(f"{FILES_DIRECTORY}/jpg", exist_ok=True)