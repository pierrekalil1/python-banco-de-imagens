from werkzeug.datastructures import FileStorage
from . import ALLOWED_EXTENSIONS, FILES_DIRECTORY
import os
from flask.helpers import send_from_directory
from werkzeug.utils import secure_filename


MAX_CONTENT_LENGTH = os.environ.get('MAX_CONTENT_LENGTH')

'''
list_all
    takes all type of image from in database

    dont use parameters 
    ----------

    returns
    ------
    returns a json list
    with all files  are in the database
'''
def list_all():
    files_list = []
    for _, _, files in os.walk(FILES_DIRECTORY):
        for file in files:
            files_list.append(file)
    return files_list, 200

'''
list_extension:
    takes a type of image and looks for occurrences of it in database

    Parameters
    ----------
    type : string

    returns
    ------
    returns a json list
    with all files of that type that are in the database
'''
def list_extension(filename: str):
    files_list = []
    for _, _, files in os.walk(f"{FILES_DIRECTORY}/{filename.split('.')[-1]}"):
        for file in files:
            files_list.append(file)

    return files_list, 200


'''
save_image:
    post a type of image and savi of it in database

    Parameters
    ----------
    type : string

    returns
    ------
    if image dont find in database, returns a message from success

    if image find in database, returns a message from failure
'''
def save_image(file: FileStorage):
    if file and allowed_file(file.filename):
        filename = secure_filename(file.filename) #segurança para o nome do arquivo

        set_list = []
        for _, _, files in os.walk(f"{FILES_DIRECTORY}/{filename.split('.')[-1]}"):
            for file_name in files:
                set_list.append(file_name)
        
        if filename in set_list:
            return {"Erro": f"File {filename} already registered in the database!"}, 400

        file.save(os.path.join(f"{FILES_DIRECTORY}/{filename.split('.')[-1]}/{filename}"))
        return {"Success": f"File {file} registered successfully!"}, 201
    else:
        return {"Failure": "Allowed only extensions: png, jpg, gif"}, 400
    

'''
allowed_file:
    verify type of extension in image 

    Parameters
    ----------
    type : string

    returns
    ------
    return a types extensions acepts in database
'''
def allowed_file(filename):
    return '.' in filename and filename.split('.')[-1] in ALLOWED_EXTENSIONS


'''
download_image:
    takes a type of image and looks for occurrences of it in database

    Parameters
    ----------
    type : string

    returns
    ------
    if image find in database, returns a download the file of that that are in the database

    if image dont find in database, returns message failure
'''
def download_image(file_name: str):
    directory_files = f"../{FILES_DIRECTORY}/{file_name.split('.')[-1]}/"
    set_list = []

    for _, _, files in os.walk(f"{FILES_DIRECTORY}/{file_name.split('.')[-1]}"):
        for file in files:
            set_list.append(file)

    if file_name not in set_list:
        return {"Error": f"Image {file_name} not found"}, 204
        
    return send_from_directory(directory=directory_files, path=file_name, as_attachment=True), 200


'''
download_zip_files:
    takes a type of extension (png, jpg, gif) image and looks for occurrences of it in database

    Parameters
    ----------
    type : string

    type: int

    returns
    ------
    if directory find in database, returns a download the directory this extensios of  that are in the database

    if directory dont find in database, returns message failure
'''
def download_zip_file(extension, compression):
    directory_files = os.listdir(path=f'{FILES_DIRECTORY}/{extension}')
    if len(directory_files) == 0:
        return {"Error": f"Empty {extension} directory"}, 204
    else:
        os.system(f'zip -r /tmp/{extension}-zip {FILES_DIRECTORY}/{extension} -{compression}')
    return send_from_directory(directory='/tmp/', path=f'{extension}-zip.zip', as_attachment=True)
    